import { h } from 'vue'
import { RouterLink } from 'vue-router'
import { NIcon } from 'naive-ui'
import { BookOutline, LaptopOutline as WorkIcon } from '@vicons/ionicons5'
function renderIcon(icon: any) {
  return () => h(NIcon, null, { default: () => h(icon) })
}

export const menuOption = [
  {
    label: '工作台',
    key: '/index/work-place',
    icon: renderIcon(BookOutline),
    renderLabel: true
  },
  {
    label: "主控台",
    key: '/index/home',
    icon: renderIcon(WorkIcon),
    renderLabel: true
  },
  {
    icon: renderIcon(WorkIcon),
    key: "/personal",
    label: "个人中心",
    renderLabel: true
  }
]