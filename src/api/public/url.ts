import { get, post } from './index'

// get 请求 样子
export function test(data: any) {
  return get('/login', data)
}

// 登录
export function login(data: any) {
  return post('/login', data)
}