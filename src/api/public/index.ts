import request from '@/api/request'

// 前台用户获取验证码
export function getVerificationCode(params: any) {
  return request({
    url: '/user/getVerificationCode',
    method: 'get',
    params
  })
}

// 获取公告
export function logins(data: any) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function get(url: any, params: any, method: any = 'GET') {
  return request({
    url,
    method,
    params
  })
}

export function post(url: any, data: any, method: any = 'POST') {
  return request({
    url,
    method,
    data
  })
}
