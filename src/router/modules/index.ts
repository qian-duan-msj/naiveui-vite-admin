import { ReorderThree } from '@vicons/ionicons5'
import {
  DashboardOutlined,
  UserOutlined,
  SettingOutlined,
  BarsOutlined,
  CreditCardOutlined,
  AppstoreOutlined,
  FileTextOutlined,
  FileProtectOutlined,
  EditOutlined,
  FileExcelOutlined,
  DragOutlined,
  ShareAltOutlined,
  EnvironmentOutlined,
  FileMarkdownOutlined
} from '@vicons/antd'
import { renderIcon } from './icons'
const Layout = () => import('@/layouts/Layout.vue')
export const userRoutes = [
  {
    path: '/index',
    component: Layout,
    name: 'Index',
    meta: {
      title: 'Dashboard',
      iconPrefix: 'iconfont',
      icon: renderIcon(DashboardOutlined),
    },
    children: [
      {
        path: 'home',
        name: 'Home',
        component: (): any => import('@/views/index/main.vue'),
        meta: {
          title: '主控台',
          affix: true,
          cacheable: true,
          icon: renderIcon(ReorderThree)
        },
      },
      {
        path: 'work-place',
        name: 'WorkPlace',
        component: (): any => import('@/views/index/work-place.vue'),
        meta: {
          title: '工作台',
          icon: renderIcon(ReorderThree)
        },
      },
    ],
  },
  {
    path: '/personal',
    name: 'personal',
    component: Layout,
    meta: {
      title: '个人中心',
      isSingle: true,
      icon: renderIcon(UserOutlined),
    },
    children: [
      {
        path: '',
        component: () => import('@/views/personal/index.vue'),
        meta: {
          title: '个人中心',
          icon: renderIcon(ReorderThree)
        },
      },
    ],
  }
]

export const rolesRoutes = [
  {
    path: '/system',
    name: 'system',
    component: Layout,
    meta: {
      title: '系统管理',
      icon: renderIcon(SettingOutlined),
    },
    children: [
      {
        path: 'department',
        name: 'systemDepartment',
        component: () => import('@/views/system/department.vue'),
        meta: {
          title: '部门管理',
          icon: renderIcon(ReorderThree),
          badge: 'new',
          cacheable: true,
        }
      },
      {
        path: 'user',
        name: 'systemUser',
        component: () => import('@/views/system/user.vue'),
        meta: {
          title: '用户管理',
          icon: renderIcon(ReorderThree),
          badge: 'dot',
        }
      },
      {
        path: 'role',
        name: 'systemRole',
        component: () => import('@/views/system/role.vue'),
        meta: {
          title: '角色管理',
          icon: renderIcon(ReorderThree),
          badge: '12',
        }
      },
      {
        path: 'menu',
        name: 'systemMenu',
        component: () => import('@/views/system/menu.vue'),
        meta: {
          title: '菜单管理',
          icon: renderIcon(ReorderThree),
          cacheable: true,
        }
      },
    ],
  },
  {
    path: '/list',
    name: 'list',
    component: Layout,
    meta: {
      title: '列表页面',
      icon: renderIcon(BarsOutlined),
    },
    children: [
      {
        path: 'table-with-search',
        name: 'listTableWithSearch',
        component: () => import('@/views/list/table-with-search.vue'),
        meta: {
          title: '表格搜索',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'table-custom',
        name: 'listTableCustom',
        component: () => import('@/views/list/table-custom.vue'),
        meta: {
          title: '自定义表格',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'list',
        name: 'listList',
        component: () => import('@/views/list/list.vue'),
        meta: {
          title: '普通列表',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'card-list',
        name: 'listCardList',
        component: () => import('@/views/list/card-list.vue'),
        meta: {
          title: '卡片列表',
          icon: renderIcon(ReorderThree),
        },
      },
    ],
  },
  {
    path: '/form',
    name: 'form',
    component: Layout,
    meta: {
      title: '表单页面',
      icon: renderIcon(CreditCardOutlined),
      badge: 'dot',
    },
    children: [
      {
        path: 'base-form-view',
        name: 'formBaseFormView',
        component: () => import('@/views/form/base-form-view.vue'),
        meta: {
          title: '基本表单',
          cacheable: true,
          icon: renderIcon(ReorderThree),
        },
      },
    ],
  },
  {
    path: '/other',
    name: 'other',
    component: Layout,
    meta: {
      title: '功能/组件',
      icon: renderIcon(AppstoreOutlined),
    },
    children: [
      {
        path: 'chart',
        name: 'otherChart',
        redirect: "/other/chart/icons",
        meta: {
          title: '图表',
          icon: renderIcon(ReorderThree),
        },
        children: [
          {
            path: 'icons',
            name: 'otherChartIcons',
            component: () => import('@/views/other/chart/icons.vue'),
            meta: {
              title: '图标',
              icon: renderIcon(ReorderThree),
            },
          },
          {
            path: 'echarts',
            name: 'otherChartEcharts',
            component: () => import('@/views/other/chart/echarts.vue'),
            meta: {
              title: 'echarts',
              icon: renderIcon(ReorderThree),
            },
          },
          {
            path: 'icon-select',
            name: 'otherChartIconSelect',
            component: () => import('@/views/other/chart/icon-select.vue'),
            meta: {
              title: '图标选择器',
              icon: renderIcon(ReorderThree),
            },
          },
        ],
      },
      {
        path: 'print',
        name: 'otherPrint',
        component: () => import('@/views/other/print.vue'),
        meta: {
          title: '打印',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'badge',
        name: 'otherBadge',
        component: () => import('@/views/other/badge.vue'),
        meta: {
          title: '消息提示',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'clipboard',
        name: 'otherClipboard',
        component: () => import('@/views/other/clipboard.vue'),
        meta: {
          title: '剪贴板',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'http://qingqingxuan.gitee.io/work-p-site',// bug 等会解决
        name: 'otherWaiLian',
        meta: {
          title: '外链',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'qrcode',
        name: 'otherQrcode',
        component: () => import('@/views/other/qrcode.vue'),
        meta: {
          title: '二维码',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'css-animation',
        name: 'otherCssAnimation',
        component: () => import('@/views/other/css-animation.vue'),
        meta: {
          title: 'CSS动画',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'flow',
        name: 'otherFlow',
        component: () => import('@/views/other/flow.vue'),
        meta: {
          title: '流程图',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'player',
        name: 'otherPlayer',
        component: () => import('@/views/other/player.vue'),
        meta: {
          title: '视频播放器',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'password-strong-page',
        name: 'otherPasswordStrongPage',
        component: () => import('@/views/other/password-strong-page.vue'),
        meta: {
          title: '密码强度',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'cropper',
        name: 'otherCropper',
        component: () => import('@/views/other/cropper.vue'),
        meta: {
          title: '图片裁剪',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'iframe',
        name: 'otherIframe',
        component: () => import('@/views/other/iframe.vue'),
        meta: {
          title: '内嵌iframe',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'big-preview',
        name: 'otherBigPreview',
        component: () => import('@/views/other/big-preview.vue'),
        meta: {
          title: '大图预览',
          icon: renderIcon(ReorderThree),
        },
      },
    ],
  },
  {
    path: '/route-params',
    name: 'routeParams',
    component: Layout,
    meta: {
      title: '路由参数',
      icon: renderIcon(FileTextOutlined),
    },
    children: [
      {
        path: 'query',
        name: 'routeParamsQuery',
        component: () => import('@/views/route-params/query-details.vue'),
        meta: {
          title: 'query参数详情',
          icon: renderIcon(ReorderThree)
        },
      },
      {
        path: 'params/:id',
        name: 'routeParamsParamsDetails',
        component: () => import('@/views/route-params/params-details.vue'),
        meta: {
          title: 'params参数详情',
          icon: renderIcon(ReorderThree)
        },
      },
    ],
  },
  {
    path: '/result',
    name: 'result',
    component: Layout,
    meta: {
      title: '结果页面',
      icon: renderIcon(FileProtectOutlined),
    },
    children: [
      {
        path: 'success',
        name: 'resultSuccess',
        component: () => import('@/views/result/success.vue'),
        meta: {
          title: '成功页面',
          cacheable: true,
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'fail',
        name: 'resultFails',
        component: () => import('@/views/result/fail.vue'),
        meta: {
          title: '失败页面',
          cacheable: true,
          icon: renderIcon(ReorderThree),
        },
      },
    ],
  },
  {
    path: '/editor',
    name: 'editor',
    component: Layout,
    meta: {
      title: '编辑器',
      badge: '12',
      icon: renderIcon(EditOutlined),
    },
    children: [
      {
        path: 'rich-text',
        name: 'editorRichText',
        component: () => import('@/views/editor/rich-text.vue'),
        meta: {
          title: '富文本',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'markdown',
        name: 'editorMarkdown',
        component: () => import('@/views/editor/markdown.vue'),
        meta: {
          title: 'markdown',
          icon: renderIcon(ReorderThree),
        },
      },
    ],
  },
  {
    path: '/excel',
    name: 'excel',
    component: Layout,
    meta: {
      title: 'Excel',
      icon: renderIcon(FileExcelOutlined),
    },
    children: [
      {
        path: 'export-excel',
        name: 'excelExportExcel',
        component: () => import('@/views/excel/export-excel.vue'),
        meta: {
          title: '导出Excel',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'export-rows-excel',
        name: 'excelExportRowsExcel',
        component: () => import('@/views/excel/export-rows-excel.vue'),
        meta: {
          title: '导出选中行',
          icon: renderIcon(ReorderThree),
        },
      },
    ],
  },
  {
    path: '/draggable',
    name: 'draggable',
    component: Layout,
    meta: {
      title: '拖拽',
      icon: renderIcon(DragOutlined),
    },
    children: [
      // {
      //   path: 'dialog-draggable',
      //   name: 'draggableDialogDraggable',
      //   component: () => import('@/views/draggable/dialog-draggable.vue'),
      //   meta: {
      //     title: '拖拽对话框',
      //     cacheable: true,
      //     icon: renderIcon(ReorderThree),
      //   },
      // },
      {
        path: 'card-draggable',
        name: 'draggableCardDraggable',
        component: () => import('@/views/draggable/card-draggable.vue'),
        meta: {
          title: '卡片拖拽',
          cacheable: true,
          icon: renderIcon(ReorderThree),
        },
      },
    ],
  },
  {
    path: '/next',
    name: 'next',
    component: Layout,
    meta: {
      title: '多级菜单',
      icon: renderIcon(ShareAltOutlined),
    },
    children: [
      {
        path: 'menu1',
        name: 'nextMenu1',
        component: () => import('@/views/next/menu1.vue'),
        meta: {
          title: 'menu-1',
          cacheable: true,
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'menu2',
        name: 'nextMenu2',
        meta: {
          title: 'menu-2',
          icon: renderIcon(ReorderThree),
        },
        children: [
          {
            path: 'menu-2-1',
            name: 'nextMenu2-1',
            meta: {
              title: 'menu-2-1',
              icon: renderIcon(ReorderThree),
            },
            children: [
              {
                path: 'menu-2-1-1',
                name: 'nextmenu-2-1-1',
                component: () => import('@/views/next/menu2/menu-2-1/menu-2-1-1.vue'),
                meta: {
                  title: 'menu-2-1-1',
                  cacheable: true,
                  icon: renderIcon(ReorderThree),
                },
              },
              {
                path: 'menu-2-1-2',
                name: 'nextmenu-2-1-2',
                component: () => import('@/views/next/menu2/menu-2-1/menu-2-1-2.vue'),
                meta: {
                  title: 'menu-2-1-2',
                  cacheable: true,
                  icon: renderIcon(ReorderThree),
                },
              },
            ],
          },
          {
            path: 'menu-2-2',
            menuName: 'menu-2-2',
            name: 'nextmenu-2-2',
            component: () => import('@/views/next/menu2/menu-2-2.vue'),
            meta: {
              title: 'menu-2-2',
              cacheable: true,
              icon: renderIcon(ReorderThree),
            },
          },
        ],
      },
    ],
  },
  {
    path: '/map',
    name: 'map',
    component: Layout,
    meta: {
      title: '地图',
      icon: renderIcon(EnvironmentOutlined),
    },
    children: [
      {
        path: 'gaode',
        name: 'mapGaode',
        component: () => import('@/views/map/gaode.vue'),
        meta: {
          title: '高德地图',
          icon: renderIcon(ReorderThree),
        },
      },
      {
        path: 'baidu',
        name: 'mapBaidu',
        component: () => import('@/views/map/baidu.vue'),
        meta: {
          title: '百度地图',
          icon: renderIcon(ReorderThree),
        },
      },
    ],
  },
  {
    path: '/project',
    name: 'project',
    component: Layout,
    meta: {
      title: '项目信息',
      isSingle: true,
      icon: renderIcon(FileMarkdownOutlined),
    },

    children: [
      {
        path: 'infomation',
        name: 'projectInfomation',
        component: () => import('@/views/project/infomation.vue'),
        meta: {
          title: '项目依赖',
          icon: renderIcon(ReorderThree)
        },
      },
    ],
  },
]


